# React Front To Back

Learn & Master React With The Context API + Redux & Build a Real World Project With Firebase/Firestore

---

In this course you will learn to master React 16.3+ concepts including the new Context API as well as Redux. We will target certain concepts while progressively building real applications. Whether you have never touched React or have been working with it for a while and want to learn more and build some cool stuff, this course is for you. The final project will be a React/Redux client management panel which uses Firebase for authentication and Firestore for storing data.

## Here are some of the things you will learn in this course:

    What is React?
    Dev Environment Setup
    ES6+ Concepts - Classes, Arrow Functions, destructuring, async/await
    Class Based & Functional Components
    JSX Syntax & Expressions
    Component Props
    Managing Component Level State
    Context API For App Level State
    Component Lifecycle
    HTTP Requests
    Redux Store Setup
    Redux Reducers & Actions
    Redux Firebase/Firestore Integration

## Quelles sont les conditions ?

    You should have a basic understanding of HTML and JavaScript

## Que vais-je apprendre grâce à ce cours ?

    Master React Concepts - Components, State, Props, etc
    Learn & Use The Context API
    Learn Redux From Scratch
    Build & Deploy a Client Management App With React, Redux & Firebase/Firestore
    Master ES6+ Features Like Arrow Functions, Spread & Async Await
    Suitable For Both Beginners & Intermediate React Developers

## Quel est le public ciblé ?

    Developers looking to learn React and Redux

[www.udemy.com/react-front-to-back](https://www.udemy.com/react-front-to-back)
